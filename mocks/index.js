module.exports = function () {
    return {
        'bands': require('./bands.json'),
        'artists': require('./artists.json'),
        'albums': require('./albums.json'),
        'tracks': require('./tracks.json'),
    };
}

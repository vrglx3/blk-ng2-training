import 'rxjs/add/operator/toPromise';

import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {environment} from '../environments/environment';


@Injectable()
export class ApiService {
    url: string = environment.api;

    constructor(private http: Http) {}


    getBands (): Promise<any> {
        return this.http.get(`${this.url}/bands`)
            .toPromise()
            .then(response => response.json());
    }

     getBand (bandId: string): Promise<any> {
         return this.http.get(`${this.url}/band/${bandId}`)
         .toPromise()
         .then(response => response.json());
     }

     getArtists (bandId: string): Promise<any> {
         return this.http.get(`${this.url}/bands/${bandId}/artists`)
         .toPromise()
         .then(response => response.json());
     }

     getAlbums (bandId: string): Promise<any> {
         return this.http.get(`${this.url}/bands/${bandId}/albums` )
         .toPromise()
         .then(response => response.json());
     }

     getAlbum (albumId: string): Promise<any> {

         return this.http.get(`${this.url}/albums/${albumId}`)
         .toPromise()
         .then(response => response.json());
     }

      getTracks (albumId: string): Promise<any> {
          return this.http.get(`${this.url}/albums/${albumId}/tracks/  `)
          .toPromise()
          .then(response => response.json());
      }
     getTrack (trackId: string): Promise<any> {
         return this.http.get(`${this.url}/tracks/${trackId}`)
         .toPromise()
         .then(response => response);
     }

     getCommentsForTrack (trackId: string): Promise<any> {
         return this.http.get(`${this.url}/tracks/${trackId}`)
         .toPromise()
         .then(response => response.json());}

     postCommentForTrack (postData: object): Promise<any>
     {
        return this.http.put(`${this.url}/tracks/${postData['id']}`, postData)
           .toPromise()
           .then( response => response.json());
     }
}

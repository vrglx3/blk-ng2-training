import {Routes} from '@angular/router';

import {
    HomePageComponent, BandPageComponent, AlbumPageComponent, CommentsPageComponent
} from './pages';


export const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomePageComponent
    },
    {
        path: 'band/:bandId',
        component: BandPageComponent,
        children: [{
            path: 'album/:albumId',
            component: AlbumPageComponent,
             children: [{
                 path: 'tracks/:trackId',
                 component: CommentsPageComponent,
             }],
        }],
    },
    {path: '**', redirectTo: '/'},
];

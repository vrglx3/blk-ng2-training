import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiService} from '../../services';



@Component({
    selector: 'comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.scss']

})
export class CommentsComponent {
    @Input() data: object;

    commentField: string;
    trackId: string;
    comment:any;
    dataResp : Array<object>;
    comments:Array<object>;


    constructor (
        private service: ApiService,
        private route: ActivatedRoute,
    ) {}


    onSubmit () {
      this.service.getTrack(this.route.snapshot.params['trackId']).then(
        (response) => {
          this.comment={"message":this.commentField, "ts":new Date().getTime()};
          this.dataResp=response.json();
          this.dataResp['comments'].push(this.comment);
          this.service.postCommentForTrack(this.dataResp).then(
            ()=>{
            document.location.reload(true);
            });
        }
      );
    }
}

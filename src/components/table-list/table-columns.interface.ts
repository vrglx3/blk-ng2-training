export interface TableColumns {
    key: string,
    displayName?: string,
    searchable?: boolean,
}

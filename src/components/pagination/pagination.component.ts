import {Component, Input, Output, EventEmitter,OnChanges} from '@angular/core';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnChanges  {
      @Input() size: number;
      @Input() data: Array<object>;
      @Output() currentDataChange = new EventEmitter(true) ;
      currentPage: number = 1;
      lastPage: number ;
      pageSize: number = 3;
      actualSize: number;
      curData  : any;
      filtered : any;
      @Input() bandfilter : string;
      @Input()
      get currentData(){
        return this.curData;
      }
      set currentData(val){
          if(val){
            this.curData = val;
            this.currentDataChange.emit(this.curData);
          }
      }
      ngOnChanges(change)
      {
        if((change['data'] || change['bandfilter'] )&& this.data){
          this.filtered=this.data;
          if(change['bandfilter']){
          this.filtered=this.filter(this.data,this.bandfilter);
          }
          this.lastPage=this.data.length/this.pageSize;
          this.currentData = this.part(this.filtered, this.currentPage, this.pageSize);
        }
      }
      part(data, currentPage, pageSize)
      {
        return data.slice((currentPage-1) * pageSize,currentPage *pageSize);
      }
      filter(dataF,filter)
      {
        var re = new RegExp(filter,'gi');
        var dataR = [];
        for(let j = 0;j<dataF.length;j++)
        {
          if(dataF[j].name.match(re))
          {
            dataR.push(dataF[j]);
          }
        }
        return dataR;
      }
      nextPage()
      {
        if(this.lastPage>this.currentPage){
          this.currentPage ++;
        }
        this.currentData = this.part(this.data, this.currentPage, this.pageSize);
      }
      prevPage()
      {
        if(this.currentPage >1){
          this.currentPage --;
        }
        this.currentData = this.part(this.data, this.currentPage, this.pageSize);
      }

}

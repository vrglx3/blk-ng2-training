import {Component,OnInit} from '@angular/core';
import {ApiService} from '../../services';

@Component({
    selector: 'home-page',
    template: `
        <input type="text" class="form-control" [(ngModel)]="Filter">
        <pre>{{Filter}}</pre>
        <table-list type="band" filter
            [paginate]="5"
            [columns]="[
                {key: 'name'},
                {key: 'genres'},
                {key: 'popularity'}
            ]"
            [data]="data | async"
            [bandfilter]="Filter">

        </table-list>
        <!-- <pre>{{data | json}}</pre> -->
    `,
})
export class HomePageComponent implements OnInit{
  data : Promise<any> ;
  Filter: string;
  constructor (
      private service: ApiService,
  ) {}
  ngOnInit(){
    this.data=this.service.getBands();
  }
}

import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import {ApiService} from '../../services';

@Component({
    selector: 'comments-page',
    templateUrl: './comments-page.component.html',
    styleUrls: ['./comments-page.component.scss']

})
export class CommentsPageComponent implements OnInit {
  trackId: string;
  subs: any;
  result : any;
  comments:any;
  constructor (
      private service: ApiService,
      private route: ActivatedRoute)
      {};

  ngOnInit () {
      this.subs=this.route.params.subscribe(params => {
      this.trackId = params['trackId'];
      this.service
      .getCommentsForTrack(this.trackId)
      .then((data) => {
        this.comments = data.comments;
      });
    });
  }
  ngOnDestroy()
  {
    this.subs.unsubscribe();
  }
}

import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import {ApiService} from '../../services';

@Component({
    selector: 'album-page',
    templateUrl: './album-page.component.html',
    styleUrls: ['./album-page.component.scss']

})
export class AlbumPageComponent implements OnInit {
  comments: Promise<any>;
  tracks: Promise<any>;
  albumId: string;
  subs: any;
  constructor (
      private service: ApiService,
      private route: ActivatedRoute)
      {
        };

  ngOnInit () {
      this.subs=this.route.params.subscribe(event => {
      this.albumId = this.route.snapshot.params['albumId'];
      this.tracks= this.service.getTracks(this.albumId);
    });
  }
  ngOnDestroy()
  {
    this.subs.unsubscribe();
  }
}

import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ApiService} from '../../services';



@Component({
    selector: 'band-page',
    templateUrl: './band-page.component.html',
    styleUrls: ['./band-page.component.scss']
})
export class BandPageComponent implements OnInit {
    artists: Promise<any>;
    albums: Promise<any>;

    constructor (
        private service: ApiService,
        private route: ActivatedRoute,
    ) {}

    ngOnInit () {
        let bandId = this.route.snapshot.params['bandId'];
        this.artists = this.service.getArtists(bandId);
        this.albums = this.service.getAlbums(bandId);
    }
}
